﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Mono.Options;
using WillowMedia.Common.IO;
using WillowMedia.Common.Pdf;
using WillowMedia.Common.Xml;

namespace WillowMedia.MergeInPdf
{
    class Program
    {
        static int Main(string[] args)
        {
            var pdf = (string) null;
            var importFile = "./merge.xml";
            var xmlHelper = new XmlHelper();
            var help = false;
            var options = new OptionSet { 
                // { "c|console", "Run in console", (n) => runInConsole = true },
//                { "d|date=", "Date of recording yyyyMMdd|today|yesterday", (n) => dateString = n },
//                { "p|prefix=", "prefix of recording", (n) => prefix = n },
//                { "o|output=", "output path", (n) => output = n }, 
                { "h|help", "show this message and exit", h => help = true },
            };

            List<string> extra;
            try {
                // parse the command line
                extra = options.Parse (args);
            } catch (OptionException e) {
                Console.WriteLine (e.Message);
                Console.WriteLine ("Try -h/--help for more information.");
                return -1;
            }

            if (help)
            {
                options.WriteOptionDescriptions(Console.Out);
                return 0;
            }

            if (!extra.IsEmpty())
                pdf = extra.FirstOrDefault();
            
            #if DEBUG
            if (pdf == null)
                pdf = "./2pages.pdf";
            
            var test = new MergeModel()
            {
                Merge = new List<Page>()
                {
                    new Page()
                    {
                        Pagenr = 2,
                        Video = "/Users/peter/Desktop/Vergadering met Peter Hagen-20210204_193220-Opname van vergadering.smaller.mp4",
                        Name = null,
                        X1 = 2.5f,
                        Y1 = 2.5f,
                        X2 = 15f,
                        Y2 = 10f
                    }
                }
            };
            
            File.WriteAllText(importFile, xmlHelper.Serialize(test));
            #endif

            if (!File.Exists(pdf))
                throw new FileNotFoundException("Input file is missing");
            if (!File.Exists(importFile))
                throw new FileNotFoundException("Import definition is missing");

            var importModel = xmlHelper.Deserialize<MergeModel>(File.ReadAllText(importFile));
            if (importModel == null)
                throw new Exception("Import model is empty");

            var tempFile = Path.GetTempFileName();
            using (var fileStream = new FileStream(tempFile, FileMode.Create))
            {
                // var doc = new Document();
                //var writer = PdfWriter.GetInstance(doc, fileStream);
                var inputPdf = PathHelpers.Normalize(pdf);
                var pdfReader = new PdfReader(inputPdf);
                var stamper = new PdfStamper(pdfReader, fileStream);

                // doc.Open();

                foreach (var merge in importModel.Merge)
                {
                    if (!string.IsNullOrEmpty(merge.Video))
                    {
                        var file = new FileInfo(PathHelpers.Normalize(merge.Video));
                        if (!file.Exists)
                            throw new FileNotFoundException(merge.Video);

                        var tag = TagLib.File.Create(file.FullName);
                        // TODO: validate tag
                        var mimetype = (string) null;
                        if (tag.GetType() == typeof(TagLib.Mpeg4.File))
                            mimetype = "video/mp4";

                        if (string.IsNullOrEmpty(mimetype))
                            throw new Exception("Unknown mimetype");

                        var name = merge.Name ?? Path.GetFileNameWithoutExtension(merge.Video);
                        var rectangle = new Rectangle(
                            Math.Min(merge.X1 ?? 0, merge.X2 ?? 0).CmToDots(),
                            Math.Min(merge.Y1 ?? 0, merge.Y2 ?? 0).CmToDots(),
                            Math.Max(merge.X1 ?? 0, merge.X2 ?? 0).CmToDots(),
                            Math.Max(merge.Y1 ?? 0, merge.Y2 ?? 0).CmToDots());

                        var fs = PdfFileSpecification.FileEmbedded(
                            stamper.Writer, merge.Video, name, null);

                        
                        stamper.AddAnnotation(
                            PdfAnnotation.CreateScreen(stamper.Writer, rectangle, name, fs, mimetype, true),
                            merge.Pagenr);
                    }
                }
                
                stamper.Close();
                pdfReader.Close();
            }
            
            File.Copy(tempFile, "./output.pdf", true);
            
            #if DEBUG
            Process.Start("open", "./output.pdf");
            #endif
            return 0;
        }
    }

    public class MergeModel
    {
        public List<Page> Merge { get; set; }
    }

    public class Page
    {
        public int Pagenr { get; set; }
        public float? X1 { get; set; }
        public float? X2 { get; set; }
        public float? Y1 { get; set; }
        public float? Y2 { get; set; }
        public string Video { get; set; }
        public string Name { get; set; }
    }
}